
# London Rumble React FrontEnd

  
## What is this ?
London Rumble is a home made card game where you play online a variant of holdem poker against friends  
The rules of this game can be found on the currently hosted server | [Rules](https://londonrumble.com/rules)
This repository contains the ReactJs front-end that is use to run the game  
Currently, the branch deploy/render is hosted and deployed over there [Londonrumble.com](https://londonrumble.com/)  
A NodeJs backend application is used to power the front and can be found  [here](https://gitlab.com/MSJarre/noderumble)  
This whole project is open source  
## Installation

To install and access this project locally, you must first clone the server repository  

    git clone git@gitlab.com:MSJarre/noderumble.git

You can then install the dependencies  

    cd noderumble
    npm install

Then launch the server  

    npm run devStart

You must then clone the front-end repository, open a new terminal  

      git clone git@gitlab.com:MSJarre/london-rumble-react-front.git

Once again install the dependencies  


    cd london-rumble-react-front
    npm install


Then start the frontend  

    npm run start
    
In development, Express server will start on port 3000, and a proxy will be made to the React front which can be started on any port you want.  
This game uses websockets to transfer information from one player to another | [socket.io](https://socket.io/)  
This game runs on a slightly modified version of poker-typescript node package | [claudijo/poker-ts](https://github.com/claudijo/poker-ts)  
The current fork of this project can be found on my github | [MSJarr/poker-ts](https://github.com/MSJarr/poker-ts)  
A small Gitlab CI already exists with ESLint and very basic tests  
Project is currently under development, and will probably be maintained if anyone shows interest  

# What needs to be done
-  Component Refacto

- Add front unit tests

- Add support for pot limit games, and Pot Limit Omaha Rumble (6 public cards, ending with 3 at showdown)