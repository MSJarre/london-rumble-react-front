import React from 'react';
import ReactDOM from 'react-dom/client';
import './public-served/index.css';
import './i18n';
import Home from './pages/Home';
import Room from './pages/Room';
import {BrowserRouter, Route, Routes} from 'react-router-dom';
import {IoProvider} from 'socket.io-react-hook';
import Navbar from './pages/Navbar';
import Rules from './pages/Rules';
import NewGame from './pages/NewGame';
import AliasSelector from './pages/AliasSelector';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
    <React.StrictMode>
      <IoProvider>
        <BrowserRouter>
          <Navbar />
          <Routes>
            <Route index element={<Home/>} />
            <Route path="room/*" element={<Room />} />
            <Route path="new-game/*" element={<NewGame />} />
            <Route path="rules/*" element={<Rules/>} />
            <Route path="alias-selector" element = {<AliasSelector/>} />
          </Routes>
        </BrowserRouter>
      </IoProvider>
    </React.StrictMode>,
);
