import React, {useState} from 'react';
import {Link, useNavigate} from 'react-router-dom';
import {useTranslation} from 'react-i18next';
import * as uuid from 'uuid';

const NewGame= () =>{
  const [depth, setDepth] = useState('100');
  const navigate = useNavigate();
  const {t} = useTranslation();
  const userName = localStorage.rumbleUserName === undefined ?
  t('unknown') :
  localStorage.rumbleUserName;
  const defaultName = t('at_x', {user: userName});


  function handleCreation() {
    const options = {
      method: 'POST',
      headers: {'Content-Type': 'application/json'},
      body: JSON.stringify({
        name: document.getElementById('gameName').value,
        identifier: uuid.v4(),
        depth: depth,
        size: 2,
        sockets: {},
        status: 'created',
      }),
    };
    fetch('http://192.168.1.27:3000/api/rooms/create', options)
        .then((res) => {
          return res.json();
        })
        .then((data) => {
          if (data.ok) {
            navigate(`/room/?id=${data.id}`);
          }
        });
  }

  function handleDepthClick(event) {
    for (const btn of document.getElementsByClassName('depth-btn')) {
      btn.classList.remove('active');
    }
    event.target.classList.add('active');
    setDepth(event.target.textContent);
  }

  return (
    <div className='welcome-page'>
      <div className='rumble-title'>
        <p>{t('create_new_game')}</p>
      </div>
      <div className="form__group gameName">
        <input type="input" autoFocus className="form__field"
          name="gameName" defaultValue={defaultName} id='gameName' spellCheck='false'/>
      </div>
      <div className='depth_info'>
        <label htmlFor="depth" className="depthLabel">{t('blinds')}</label>
        <div className='depth-btns'>
          <button className='depth-btn' onClick={handleDepthClick}>25</button>
          <button className='depth-btn' onClick={handleDepthClick}>50</button>
          <button className='depth-btn active' onClick={handleDepthClick}>100</button>
          <button className='depth-btn' onClick={handleDepthClick}>200</button>
          <button className='depth-btn' onClick={handleDepthClick}>500</button>
        </div>
      </div>
      <div className='home-page-buttons'>
        <button className='start-game' id='join-room' onClick={handleCreation}>
          {t('lets_go')}
        </button>
        <Link to={'/'} className='start-game' id='back-to-lobby'>{t('lobby')}</Link>
      </div>
    </div>
  );
};
export default NewGame;
