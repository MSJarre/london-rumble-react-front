import React, {useState} from 'react';
import {useTranslation} from 'react-i18next';
import {Link} from 'react-router-dom';

const NavBar= () =>{
  const {t, i18n} = useTranslation();
  const [soundBool, setSoundBool] = useState(!!localStorage.rumbleSound);

  function updateSoundBoolean() {
    localStorage.rumbleSound = soundBool ? '' : soundBool;
    setSoundBool(!soundBool);
  }


  const handleLanguageChange = (language) => {
    i18n.changeLanguage(language);
  };

  return (
    <div className='navBar'>
      <div className='navBarItem'>
        <img className='flag' id='en' alt='EN' onClick={() => handleLanguageChange('en')}
          src={require('../flags/en.png')} />
        <img className='flag' id='fr' alt='FR' onClick={() => handleLanguageChange('fr')}
          src={require('../flags/fr.png')} />
        <img className='flag sound' id='sound' alt='SON' onClick={updateSoundBoolean}
          src={ soundBool ?
        require('../public-served/sound.png') :
        require('../public-served/no_sound.png')}/>
      </div>
      <div className='navBarItem'>
        <Link to={'/rules/?lang='+i18n.language} target="_blank">{t('rules_of_the_game')}</Link>
      </div>
      <div className='navBarItem'>
        <a href='https://gitlab.com/MSJarre/noderumble' target="_blank" rel="noreferrer">
          {t('contribute')}
        </a>
      </div>
    </div>
  );
};
export default NavBar;
