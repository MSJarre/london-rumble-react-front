import React, {useState, useEffect, useRef} from 'react';
import {useTranslation} from 'react-i18next';
import {useSocket} from 'socket.io-react-hook';
import UxUtil from './Room/UxUtil';
import WaitingPopup from './Room/WaitingPopup';
import Rematch from './Room/Rematch';
import JokerPopup from './Room/JokerPopup';
import DiscardMessage from './Room/DiscardMessage';
import HeroActionButtons from './Room/HeroActionButtons';
import HeroDash from './Room/HeroDash';
import VillainDash from './Room/VillainDash';
import CommunityDash from './Room/CommunityDash';
import OpponentJokerMessage from './Room/OpponentJokerMessage';


// NEED  REFACTO

function Room() {
  // Defining all hooks
  const {t} = useTranslation();
  const [foldbtn, setFoldbtn] = useState(t('fold'));
  const [callbtn, setCallbtn] = useState(t('call'));
  const [betbtn, setBetbtn] = useState(t('bet'));
  const [VillainStack, setVillainStack] = useState('1000');
  const [HeroStack, setHeroStack] = useState('1000');
  const [HeroPrivate, setHeroPrivate] = useState('back');
  const [rumbleId, setRumbleId] = useState(0);
  const [villainId, setVillainId] = useState(1);
  const [VillainPublic, setVillainPublic] = useState(Array(4).fill('back'));
  const allInRef = useRef(false);
  const [HeroPublic, setHeroPublic] = useState(Array(4).fill('back'));
  const heroPublicReference = useRef(HeroPublic);
  const discardReference = useRef(null);
  const jokerReference = useRef(false);
  const [VillainPrivate, setVillainPrivate] = useState('back');
  const [HeroBet, setHeroBet] = useState('');
  const [VillainBet, setVillainBet] = useState('');
  const [Board, setBoard] = useState(Array(5).fill('15_0'));
  const [potSize, setPotSize] = useState(0);
  const [bigBlind, setBigBlind] = useState(null);
  const [popupMessage, setPopupMsg] = useState('waiting_for_opponent');


  // Incorporate room id as a Socket header
  const {socket} = useSocket({
    extraHeaders: {
      'room_id': UxUtil.getRoomId(),
      'user_id': localStorage.rumbleUserName,
    },
  });

  // Update Bet Btn value on wheel roll
  function handleWheel(event) {
    setBetbtn(UxUtil.handleWheel(event, betbtn, bigBlind));
  }

  /*
        On action needed
        @Params event: {index: Integer, legalActions: Array[Integer]}
    */
  function handleActionNeeded(data) {
    UxUtil.highlightActivePlayer(data, rumbleId);
    // If discard time ->  Display discard message and change style
    if (data.index === rumbleId && data.legalActions === 'discard') {
      UxUtil.playSound('notification');
      setHeroPublic(HeroPublic.map((x, i) => data.discarded.includes(i + 1) ? '15_0' : x));
      UxUtil.allowDiscard();
    } else {
      // Else update buttons and hero-btns styles
      if (data.index === rumbleId) {
        UxUtil.playSound('notification');
        updateActionButtons(data);
        document.getElementsByClassName('hero-actions-btn')[0].style.display = 'flex';
      } else {
        document.getElementsByClassName('hero-actions-btn')[0].style.display = 'none';
      }
    }
  }

  /*
        When opponent disconnects
    */
  function handlePlayerDisconnected() {
    // Display waiting popup, unless rematch popup is up
    if (document.getElementById('rematch').style.display === '') {
      document.getElementById('waitingpopup').hidden = false;
    }
  }

  /*
        On user login
        @Params index: Integer
    */
  function handleLogin(data) {
    // Set index = seatIndex inside sessionStorage
    setRumbleId(data.index);
    setVillainId(data.index === 0 ? 1 : 0);
    setBigBlind(data.blind);
    if (data.toStart) {
      socket.emit('startGame');
    }
  }

  /*
        When new action arrives, update buttons
        @Params data: {}
    */
  function updateActionButtons(data) {
    // Get Range value and set them for range and box
    const range = data.legalActions.chipRange;
    UxUtil.updateRaiseValues(range);
    // Set static buttons
    setFoldbtn(t(data.legalActions.actions[0]));
    setCallbtn(t(data.legalActions.actions[1]));
    // Set Bet btn (disabled if impossible)
    if (data.legalActions.actions[2] !== undefined) {
      setBetbtn(t(data.legalActions.actions[2])+' '+ range.min);
    } else {
      setBetbtn(t('raise'));
      document.getElementById('raisebtn').classList.add('inactive-btn');
    }
  }

  /*
        Update community cards
        @Params data: {}
    */
  function communityCardsUpdate(data, allIn, street=false) {
    const board = data._cards.map( (x) => (x.rank+'_'+x.suit));
    // Use cards and fill with empty
    const currentBoardSize = Board.filter((card) => card !== '15_0').length;
    if (!allIn || currentBoardSize >= 4) {
      if (board.length !== 0 && board.length !== Board.filter((c) => c !== '15_0').length) {
        UxUtil.playSound('card');
      }
      setBoard(board.concat((Array(5 - data._cards.length).fill('15_0'))));
    } else {
      allInRef.current = true;
      if (currentBoardSize === 0 && !street) {
        setBoard(board.slice(0, 3).concat(Array(2).fill('15_0')));
        setTimeout(() => communityCardsUpdate(data, true, 'flop'), '1500');
      } else if ((currentBoardSize === 3 && !street) || street === 'flop') {
        setBoard(board.slice(0, 4).concat(Array(1).fill('15_0')));
        setTimeout(() => communityCardsUpdate(data, true, 'turn'), '1500');
      } else if (currentBoardSize === 4 || street === 'turn') {
        setBoard(board);
      }
      UxUtil.playSound('card');
    }
  }

  /*
        Put button at the correct place
        @Params hero: Int button: Int
    */
  function displayButton(hero, button) {
    document.getElementById('hero-button-img').style.display = hero === button ? 'flex' : 'none';
    document.getElementById('villain-button-img').style.display = hero === button ? 'none' : 'flex';
  }

  /*
        Display joker popup
    */
  function offerJokerChange() {
    jokerReference.current = true;
    document.getElementById('jokerpopup').style.display = 'block';
  }

  function handleOpponentChangeJoker() {
    jokerReference.current = true;
    document.getElementById('opponent-joker-msg').style.display = 'flex';
  }

  /*
        Set all hand details
        @Params data: {}
    */
  function setHandStatus(data, allIn) {
    // Set all values with hooks
    setPotSize(data.potSize);
    displayButton(rumbleId, data.button);
    setHeroPrivate(data.players[rumbleId]._private.rank+'_'+data.players[rumbleId]._private.suit);
    setVillainPublic(data.players[villainId]._public.map( (x) => (x.rank+'_'+x.suit)));
    setVillainBet(data.players[villainId]._betSize);
    setHeroBet(data.players[rumbleId]._betSize);
    setHeroStack(data.players[rumbleId]._total - data.players[rumbleId]._betSize);
    setVillainStack(data.players[villainId]._total - data.players[villainId]._betSize);
    communityCardsUpdate(data.community, allIn);
  }

  /*
        Reset hooks, reset card pointer event
    */
  function resetHand() {
    setHeroPublic(Array(4).fill('back'));
    setVillainPrivate('back');
    jokerReference.current = false;
    discardReference.current = null;
    allInRef.current = false;
    for (const card of document.getElementsByClassName('hpcard')) {
      card.style.pointerEvents = 'auto';
    }
  }

  /*
        Display everyone holecards --> Showdown every hand
        @Params data: {}
    */
  function displayHoleCards(data) {
    // Hide joker Change message
    document.getElementById('opponent-joker-msg').style.display = 'none';
    const lastDic = discardReference.current;
    // Hooks to set values
    const heroPublic = data[rumbleId].slice(1, 5).map((x, i) => x.rank+'_'+x.suit);
    if (!!lastDic && !jokerReference.current && rumbleId === lastDic.lastDiscarded) {
      heroPublic[lastDic.index - 1] = lastDic.card.rank+'_'+lastDic.card.suit;
    }
    setVillainPrivate(data[villainId][0].rank +'_'+data[villainId][0].suit);
    setHeroPublic(heroPublic);
    setTimeout(() => {
      if (rumbleId === 0) {
        socket.emit('showdownDisplayOver');
      }
    }, allInRef.current ? '7000' : '4000');
  }

  /*
        Display the discarded card after click
        @Params data: {}
    */
  function displayDiscarded(data) {
    // Get card alias
    const card = data.card.rank+'_'+data.card.suit;
    // Set it public with it's index
    if (data.card.rank >= 13) {
      UxUtil.playSound('oops');
    }
    setHeroPublic(HeroPublic.map((x, i) => i === parseInt(data.index) - 1 ? card : x));
    // Throw it to the muck
    document.getElementById('card'+data.index).classList.add('to-muck');
    // Set it to blank and back to the hand after a second
    if ({}.propertyIsEnumerable.call(data, 'lastDiscarded')) {
      discardReference.current = data;
    }
    setTimeout(() => {
      setHeroPublic(heroPublicReference.current.map((x, i) => i === data.index - 1 ? '15_0' : x));
      document.getElementById('card'+data.index).classList.remove('to-muck');
    }, '1000');
  }

  useEffect(() => {
    heroPublicReference.current = HeroPublic;
  }, [HeroPublic]);

  useEffect(() => {
    socket.on('handStatus', (data, allIn) => {
      setHandStatus(data, allIn);
    });
    socket.on('resetHand', () => {
      resetHand();
    });
    socket.on('changeJoker', () => {
      offerJokerChange();
    });
    socket.on('opponentChangeJoker', () => {
      handleOpponentChangeJoker();
    });
    socket.on('showdown', (data) => {
      displayHoleCards(data);
    });
    socket.on('displayDiscarded', (data) => {
      displayDiscarded(data);
    });
    socket.on('login', (data) => {
      handleLogin(data);
    });
    socket.on('loginFail', (msg) => {
      setPopupMsg(msg);
    });
    socket.on('playerDisconnected', () => {
      handlePlayerDisconnected();
    });
    socket.on('communityCardsUpdate', (data) => {
      communityCardsUpdate(data);
    });
    socket.on('opponentFolded', () => {
      setVillainBet(t('fold'));
    });
    socket.on('actionNeeded', (data) => {
      handleActionNeeded(data);
    });
    socket.on('disconnect', () => {
      socket.off('connect');
    });

    return () => {
      // Stop listening on return
      const socketListeners = ['connect', 'disconnect', 'login', 'actionNeeded',
        'communityCardsUpdate', 'handStatus', 'playerDisconnected', 'resetHand',
        'changeJoker', 'showdown', 'opponentFolded', 'displayDiscarded', 'opponentChangeJoker'];
      for (let i = 0; i < socketListeners.length; i++) {
        socket.off(socketListeners[i]);
      }
    };
  });

  return (
    <div className='poker-room' onWheel={handleWheel}>
      <div className='player-interface'>
        <div className='circle'>
          <div className='table-components'>
            <VillainDash VillainBet={VillainBet} VillainPrivate={VillainPrivate}
              VillainPublic={VillainPublic} VillainStack={VillainStack} UxUtil={UxUtil}/>
            <div id='villain-button' className='button-row'>
              <img id='villain-button-img' alt='B' className='villain-button button'
                src={require('../cards/dealer.png')}/>
            </div>
            <CommunityDash Board={Board} potSize={potSize} UxUtil={UxUtil}/>
            <div id='hero-button' className='button-row'>
              <img id='hero-button-img'className='hero-button button' alt='B'
                src={require('../cards/dealer.png')}/>
            </div>
            <HeroDash HeroPrivate={HeroPrivate} HeroPublic={HeroPublic} HeroStack={HeroStack}
              HeroBet={HeroBet} socket={socket} rumbleId={rumbleId} UxUtil={UxUtil}/>
          </div>
        </div>
        <HeroActionButtons betbtn={betbtn} foldbtn={foldbtn} callbtn={callbtn} socket={socket}/>
      </div>
      <DiscardMessage/>
      <OpponentJokerMessage/>
      <Rematch socket={socket} rumbleId={rumbleId}/>
      <JokerPopup socket={socket} rumbleId={rumbleId} UxUtil={UxUtil}/>
      <WaitingPopup message={popupMessage}/>
    </div>
  );
}

export default Room;
