import React from 'react';
import {useEffect} from 'react';
import {useTranslation} from 'react-i18next';
import i18n from '../i18n';

const Rules= () =>{
  const {t} = useTranslation();


  useEffect(() => {
    const params = new Proxy(new URLSearchParams(window.location.search), {
      get: (searchParams, prop) => searchParams.get(prop),
    });
    i18n.changeLanguage(params.lang);
  }, []);

  return (
    <div className='welcome-page'>
      <div className='rules-title'>
        <p>{t('rumble_rules')}</p>
      </div>
      <div className="rules">
        <p className='rules-block-label'>{t('setup')}</p>
        <div className='rules_block'>
          <p className='rules-oneliner'>{t('nlh_but')}</p>
          <div className='rules_illustration'>
            <p>{t('two_jokers')}</p>
            <img className='rules-card' alt='J' src={require('../cards/13_0.png')} />
            <img className='rules-card' alt='J' src={require('../cards/13_1.png')} />
            <p>{t('rules_added')}</p>
            <img className='rules-card' alt='R' src={require('../cards/14_0.png')} />
            <p>{t('are_added')}</p>
          </div>
          <div className='rules_illustration'>
            <p>{t('one_private_card')}</p>
            <img className='rules-card' alt='V' src={require('../cards/9_2.png')} />
            <p>{t('four_public_cards')}</p>
            <img className='rules-back-card first' alt='B' src={require('../cards/back.png')} />
            <img className='rules-back-card' alt='B' src={require('../cards/back.png')} />
            <img className='rules-back-card' alt='B' src={require('../cards/back.png')} />
            <img className='rules-back-card last' alt='B' src={require('../cards/back.png')} />
          </div>
          <p className='rules-oneliner'>{t('you_can_see')}</p>
        </div>

        <p className='rules-block-label'>{t('in_game')}</p>
        <div className='rules_block'>
          <p className='rules-oneliner'>{t('betting_round')}</p>
          <p className='rules-oneliner'>{t('after_betting')}</p>
          <div className='rules_illustration'>
            <p>{t('showdown_cards')}</p>
            <img className='rules-back-card first' alt='V' src={require('../cards/9_2.png')} />
            <img className='rules-back-card last' alt='B' src={require('../cards/back.png')} />
          </div>
        </div>

        <p className='rules-block-label'>{t('showdown')}</p>
        <div className='rules_block'>
          <div className='rules_illustration'>
            <p>{t('a_joker')}</p>
            <img className='rules-card' alt='J' src={require('../cards/13_0.png')} />
            <p>{t('a_joker_becomes')}</p>
          </div>
          <div className='rules_illustration'>
            <p>{t('a_joker_can_be')}</p>
            <img className='rules-back-card first' alt='As' src={require('../cards/12_3.png')} />
            <img className='rules-back-card last' alt='As' src={require('../cards/12_3.png')} />
            <p>{t('a_joker_makes_flushes')}</p>
          </div>
          <div className='rules_illustration'>
            <p>{t('winning_with_rules_0')}</p>
            <img className='rules-card' alt='R' src={require('../cards/14_0.png')} />
            <p>{t('winning_with_rules_1')}</p>
          </div>
          <div className='rules_illustration'>
            <p>{t('rules_showdowning')}</p>
            <img className='rules-card' alt='R' src={require('../cards/14_0.png')} />
            <p>{t('rules_as_public')}</p>
          </div>
          <div className='rules_illustration'>
            <p>{t('rules_showdowning')}</p>
            <img className='rules-card' alt='R' src={require('../cards/14_0.png')} />
            <p>{t('rules_as_private')}</p>
          </div>
        </div>
      </div>
    </div>
  );
};
export default Rules;
