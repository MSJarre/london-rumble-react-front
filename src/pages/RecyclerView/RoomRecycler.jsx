import React from 'react';
import {FlatList, StyleSheet, Text, View} from 'react-native';
import LabelItem from './labelItem';
import ListItem from './listItem'
import RoomFilters from './RoomFilters';
import { useTranslation } from 'react-i18next';


const styles = StyleSheet.create({
  container: {
    height: '100%',
    maxHeight: '100%',
    width: '80vw',
  }
});

const RoomRecycler = (roomData) => {
  const { t } = useTranslation();

  return (
    <div id='recyclerView' className='recycler'>
      <View style={styles.container}>
      <RoomFilters/>
      <FlatList
        style={{ flexDirection: "column", flexShrink: 0}}
        data={[""]}
        renderItem={({ item }) => <LabelItem data={item} />}
      />
      { roomData.data.length !== 0 ?
      <FlatList id='MyFlatListID'
        keyExtractor={item => item.id}
        data={roomData.data}
        renderItem={({item}) => <ListItem data={item}></ListItem>}
      /> 
      : <div className='roomItemList'>
          <p className='listItem listItemFullSize'>{t('no_games')}</p>
        </div>
      }
    </View>
    </div>
    
  );
};

export default RoomRecycler;