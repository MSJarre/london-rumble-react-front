import React from 'react';
import { useTranslation } from 'react-i18next';
import {useNavigate } from 'react-router-dom';



const ListItem = (roomItem) => {
  const { t } = useTranslation();
  const navigate = useNavigate();

  function handleJoinGame(event) {
    navigate(`/room/?id=${event.target.getAttribute('roomid')}`)
  }

  var item = roomItem.data
  return (
    <div className='roomItemList'>
        <p className='listItem listItemName'>{item.name}</p>
        <p className='listItem'>{parseInt(500/item.depth)} / {parseInt(1000/item.depth)}</p>
        <div className='listItem item-join-c'>
          { item.status === 'waiting'
          ? 
          <button roomid={item.identifier} className='itemListJoin join-game' onClick={handleJoinGame}>{t('join')}</button>
          :
          <p className='listItem listItemName'>{t(item.status)}</p> }
        </div>
    </div>
  )
};

export default ListItem;