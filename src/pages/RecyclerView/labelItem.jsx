import React from 'react';
import { useTranslation } from 'react-i18next';


const LabelItem = () => {
  const { t } = useTranslation();
  return (
    <div className='roomLabelItemList'>
        <p className='listItem listItemName'>{t('name')}</p>
        <p className='listItem'>{t('blinds')}</p>
        <p className='listItem '>{t('status')}</p>
    </div>
  )
};

export default LabelItem;