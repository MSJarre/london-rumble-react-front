import React, { useEffect } from 'react';
import { useTranslation } from 'react-i18next';


const RoomFilters = () => {
  const { t } = useTranslation();

  function handleChecking() {
    var statuses = []
    for (let box of document.getElementsByClassName('statusFilter')) {
      if (box.checked) {
        statuses.push(box.getAttribute('filter'))
      }
    }
    var filterDiv = document.getElementById('statusFilterVal')
    filterDiv.innerHTML = `${statuses.join(",")}`
  }

  // Use Effect hook to check on Render boxes if preferences exists
  useEffect(() => {
    var prefStatus = localStorage.rumbleStatusFilter
    if (!!prefStatus) {
      var toCheck = prefStatus.split(',')
      for (let box of document.getElementsByClassName('statusFilter')) {
        if (toCheck.includes(box.getAttribute('filter'))) {
          box.checked = true
        } else {
          box.checked = false
        }
      }
    }
  }, []);

  return (
    <div className='roomFilters'>
        <div className='roomFilter'>
          <p className='filterLabel'>{t('waiting')}</p>
          <input onChangeCapture={handleChecking} className='statusFilter' filter='waiting' type="checkbox" defaultChecked="true"></input>
        </div>
        <div className='roomFilter'>
          <p className='filterLabel'>{t('playing')}</p>
          <input onChange={handleChecking} className='statusFilter' filter='playing' type="checkbox" defaultChecked="true"></input>
        </div>
        <div className='roomFilter'>
          <p className='filterLabel'>{t('over')}</p>
          <input onChange={handleChecking} className='statusFilter' filter='over' type="checkbox"></input>
        </div>
        <div className='roomFilter'>
          <p className='filterLabel'>{t('cancelled')}</p>
          <input onChange={handleChecking} className='statusFilter' filter='cancelled' type="checkbox" ></input>
        </div>
    </div>
  )
};

export default RoomFilters;