import React, {useState, useEffect} from 'react';
import {useTranslation} from 'react-i18next';
import {Link, useNavigate} from 'react-router-dom';
import {useSocket} from 'socket.io-react-hook';
import RoomRecycler from './RecyclerView/RoomRecycler';
import '../App.scss';


function App() {
  const {t} = useTranslation();
  const [roomData, setRoomData] = useState({});
  const navigate = useNavigate();

  // Check userName
  checkRumbleName();

  // Subscribe to DatabaseObserver
  const {socket} = useSocket({
    extraHeaders: {
      'room_observer': true,
    },
  });

  // Use Effect hook to fetch Room Data for display
  useEffect(() => {
    const observer = new MutationObserver(function(mutationsList, observer) {
      localStorage.rumbleStatusFilter = document.getElementById('statusFilterVal').innerHTML;
      fetchRoomData();
    });

    observer.observe(document.getElementById('statusFilterVal'),
        {characterData: false, childList: true, attributes: false});
    fetchRoomData();
  }, []);

  // Use Effect to subscribe/unsubscribe to Room
  useEffect(() => {
    socket.on('roomUpdate', () => {
      fetchRoomData();
    });

    return () => {
      socket.off('roomUpdate');
    };
  }, [socket]);

  function checkRumbleName() {
    if (!localStorage.rumbleUserName) {
      navigate(`/alias-selector`);
    }
  }

  function fetchRoomData() {
    if (!localStorage.rumbleStatusFilter) {
      localStorage.rumbleStatusFilter = 'waiting,playing,over';
    }
    fetch(`http://localhost:3000/api/rooms?status=${localStorage.rumbleStatusFilter}`, {
      headers: {
        'accepts': 'application/json',
      },
    }).then((res) => {
      return res.json();
    }).then((json) => setRoomData(json.data))
        .catch( (a) => {
          console.log(a);
        });
  }

  return (
    <div className='welcome-page'>
      <div className='rumble-title'>
        <p>{t('welcome_to_rumble')}</p>
      </div>
      <RoomRecycler data={roomData}/>
      <div className='home-page-buttons'>
        <Link to={'/new-game'} className='start-game' id='back-to-lobby'>{t('start_game')}</Link>
      </div>
      <div className='statusFilterVal' id='statusFilterVal'>waiting</div>
    </div>

  );
}

export default App;
