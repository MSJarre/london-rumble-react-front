import React from 'react';
import { useTranslation } from 'react-i18next';
import {useNavigate } from 'react-router-dom';



const ChatItem = (chatItem) => {
  const { t } = useTranslation();
  var item = chatItem.data
  var isUs = item.user === localStorage.rumbleUserName
  return (
    <div className='chatData'>
      <p className={ isUs ? 'chatUser' : 'chatOpponent'}>{item.user}</p>
      <p className='chatMessage'>{item.text}</p>
    </div>

  )
};

export default ChatItem;