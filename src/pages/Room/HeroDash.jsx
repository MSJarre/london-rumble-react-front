import React, {useEffect} from 'react';

const HeroDash= (props) =>{

  /*
    WS Emit when a client discards a card
    @Params event: Click(card)
  */
  function HandleDiscard(event) {
    // Only if the card is clickable
    if (event.currentTarget.classList.contains('clickable')) {
      var publicCards = document.getElementsByClassName("hero-public");
      // Other cards aren't clickable anymore
      for (var i = 0; i < publicCards.length; i++) {
        publicCards[i].firstChild.classList.remove('clickable');
      }
      // Style corrections and we emit
      document.getElementById("discard-msg").style.display = 'none'
      event.currentTarget.style.pointerEvents = 'none'
      props.socket.emit('discarded', {
        seat: props.rumbleId,
        index: event.currentTarget.parentElement.id
      });
    }
  };

  useEffect(() => {
    document.getElementById('hBetV').classList.toggle("transparent", props.HeroBet === 0);
  }, [props.HeroBet]);

  return (
    <div className="hero-dash dash">
    <table>
      <tbody>
        <tr>
          <td colSpan='5' className='hero-bet bet'>
            <p id='hBetV'>{props.HeroBet}</p>
          </td>
        </tr>
        <tr id='hero-cards' className='hero-cards'>
          <td className='hero-private private'>
            <img className='card' alt={props.HeroPrivate} src={props.UxUtil.cardSrc(props.HeroPrivate)} />
          </td>
          <td id='1' className='hero-public public'>
            <img id ='card1' className='cardcol1 card card0 hpcard' alt={props.HeroPublic[0]} data-discard={props.HeroPublic[0]} onClick={HandleDiscard} src={props.UxUtil.cardSrc(props.HeroPublic[0])} />
          </td>
          <td id='2' className='hero-public public'>
            <img id ='card2' className='cardcol1 card card1 hpcard' alt={props.HeroPublic[1]} data-discard={props.HeroPublic[1]} onClick={HandleDiscard} src={props.UxUtil.cardSrc(props.HeroPublic[1])} />
          </td>
          <td id='3' className='hero-public public'>
            <img id ='card3' className='cardcol2 card card2 hpcard' alt={props.HeroPublic[2]} data-discard={props.HeroPublic[2]} onClick={HandleDiscard} src={props.UxUtil.cardSrc(props.HeroPublic[2])} />
          </td>
          <td id='4' className='hero-public public'>
            <img id ='card4' className='cardcol2 card card3 hpcard' alt={props.HeroPublic[3]} data-discard={props.HeroPublic[3]} onClick={HandleDiscard} src={props.UxUtil.cardSrc(props.HeroPublic[3])} />
          </td>
        </tr>
        <tr>
          <td colSpan='5' className='hero-stack stack'>
            <p>{props.HeroStack}</p>
          </td>
        </tr>
      </tbody>
    </table>
  </div>
  );
}
export default HeroDash;