var cardDic = {}
  for (let i = 0; i < 13; i++) {
    for (let j = 0; j < 4; j++) {
      cardDic[`${i}_${j}`] = require(`../../cards/${i}_${j}.png`)
    }
  }
  for (let card of ['13_0', '13_1', '14_0', '15_0']) {
    cardDic[card] = require(`../../cards/${card}.png`)
  }
  cardDic['back'] = require(`../../cards/back.png`)

class UxUtil {

  /*
  Plays sound
  @Params sound: <String>
  */
  static playSound(sound) {
    if (!!localStorage.rumbleSound) {
      new Audio(require(`../../public-served/${sound}.wav`)).play()
    }
  }

  /*
    Put light on the player to act
    @Params data: {index: Integer}
  */
  static highlightActivePlayer(data, rumbleId) {
    document.getElementById('waitingpopup').hidden = true
    var active = rumbleId === data.index ? 'hero' : 'villain'
    var inactive = active === 'hero' ? 'villain' : 'hero'
    document.getElementById(active+'-cards').classList.add('highlighted')
    document.getElementById(inactive+'-cards').classList.remove('highlighted')
  }

  static allowDiscard() {
    document.getElementById("discard-msg").style.display = 'flex'
    var publicCards = document.getElementsByClassName("hero-public");
    for (var i = 0; i < publicCards.length; i++) {
        // Set the cards clickables
        if (publicCards[i].firstChild.getAttribute('data-discard') === 'back'){
            publicCards[i].firstChild.classList.add('clickable');
        }
    }
  }

  static getRoomId() {
    // Get the room_id from the url
    const params = new Proxy(new URLSearchParams(window.location.search), {
      get: (searchParams, prop) => searchParams.get(prop),
    });
    return params.id
  }

  /*
    When user turns its wheel up or down, update betSize
    @Params event: [WheelEvent]
  */
  static handleWheel(event, betbtn, blind) {
    if (!event.target.className.match(/css-view|chatData/)) {
      var rangeValue = parseInt(document.getElementById('raiseRange').value);
      var rangeMax = parseInt(document.getElementById('raiseRange').max);
      if (rangeMax !== 0 && document.getElementsByClassName("hero-actions-btn")[0].style.display === 'flex') {
        var toAdd = event.deltaY > 0 ? -1 * blind : blind
        var newValue = 0
        if (toAdd < 0 ) {
            newValue = Math.max(rangeValue + toAdd, document.getElementById('raiseRange').min)
        } else {
            newValue = Math.min(rangeValue + toAdd, document.getElementById('raiseRange').max)
        }
        var newBetnBtn = betbtn.replace(/[0-9]+/, '')
        var target = document.getElementById('raiseValue')
        target.value = newValue
        var minValue = parseInt(target.min)
        var sentVal = newValue >= minValue ? Math.min(newValue, parseInt(target.max)) : minValue
        document.getElementById('raiseRange').value = sentVal
        return newBetnBtn + sentVal
     }
    } else {
      return betbtn
    }
  }

  static updateRaiseValues(range) {
    document.getElementById('raiseRange').min = range.min
    document.getElementById('raiseRange').max = range.max
    document.getElementById('raiseValue').min = range.min
    document.getElementById('raiseValue').max = range.max
    document.getElementById('raiseRange').value = range.min
    document.getElementById('raiseValue').value = ''
    document.getElementById("raisebtn").classList.remove("inactive-btn")
  }

  static cardSrc(card) {
    return cardDic[card]
  }

}

export default UxUtil;