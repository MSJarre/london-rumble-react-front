import React from 'react';
import { useTranslation } from 'react-i18next';

const CommunityDash= (props) =>{
  const { t } = useTranslation();
  
  return (
  <div className='row communityCards'>
    <div className='comm-card comm-0'>
      <img className='card' alt={props.Board[0]} src={props.UxUtil.cardSrc(props.Board[0])} />
    </div>
    <div className='comm-card comm-1'>
      <img className='card' alt={props.Board[1]} src={props.UxUtil.cardSrc(props.Board[1])} />
    </div>
    <div className='comm-card comm-2'>
      <img className='card' alt={props.Board[2]} src={props.UxUtil.cardSrc(props.Board[2])} />
    </div>
    <div className='comm-card comm-3'>
      <img className='card' alt={props.Board[3]} src={props.UxUtil.cardSrc(props.Board[3])} />
    </div>
    <div className='comm-card comm-4'>
      <img className='card' alt={props.Board[4]} src={props.UxUtil.cardSrc(props.Board[4])} />
    </div>
    <div className='pot-size'>
      <p>{t('pot')}: {props.potSize}</p>
    </div>
  </div>
  );
}
export default CommunityDash;