import React from 'react';
import { useTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';


const WaitingPopup= (props) =>{
  const { t } = useTranslation();

  return (
    <div id="waitingpopup" className="overlay overlay-room">
        <div className="popup">
            <div className="content">{t(props.message)}</div>
            <div className='popupLinkDiv'>
              <Link to={'/'} className='popupLink'>{t('lobby')}</Link>
            </div>
        </div>
        <div className="flexdots">
            <div className="dot-carousel"/>
        </div>
    </div> 
  );
}
export default WaitingPopup;