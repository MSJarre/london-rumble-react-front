import React, {useState, useEffect} from 'react';
import { useTranslation } from 'react-i18next';
import {FlatList} from 'react-native';
import ChatItem from './ChatItem';

function HeroActionButtons(props) {
  const [myBetBtn, setBetbtn] = useState(`${props.betbtn}`)
  const [chatMessages, setChatMessages] = useState([])
  const { t } = useTranslation();

  // Reload betBtn value on Render and update it from property
  useEffect(() => {
    setBetbtn(props.betbtn);
  }, [props.betbtn])


  useEffect(() => {
    props.socket.on('NewMessage', (data) => {
        console.log(data);
        setChatMessages(chatMessages.concat([data]))
    })

    props.socket.on('login', (data) => {
      var chatMsg = {
        text: t('login_from', {user: data.user_id}),
        user: data.user_id
      }
      props.socket.emit("sentMessage", chatMsg)
      setChatMessages(data.conversation.concat([chatMsg]))
    })

    return () => {
      props.socket.off('NewMessage')
      props.socket.off("login")
    };
  });

  useEffect(() => {
    var element = document.getElementById('chatTextBox')
    element.scrollTop = element.scrollHeight
  }, [chatMessages])

  /*
    When user clicks action button
    @Params event: [Click]
  */
  function handleAction(event) {
    // Get clicked action
    var action = event.currentTarget.getAttribute('data-action')
    // Get amount
    var raiseRange = document.getElementById('raiseRange')
    var amount = parseInt(raiseRange.value)
    // Emit both
    props.socket.emit("action", action, amount)
    // Hide buttons
    document.getElementsByClassName("hero-actions-btn")[0].style.display = "none";
  }

  function handleSendMessage() {
    var myText = document.getElementById('chatInput').value
    if (!!myText) {
      var data = {
        text: myText,
        user: localStorage.rumbleUserName
      }
      props.socket.emit("sentMessage", data)
      setChatMessages(chatMessages.concat([data]))
      document.getElementById('chatInput').value = ''
    }
  }

  function handleKeyPress(event) {
    if (event.keyCode === 13) {
        handleSendMessage()
    }
  }

    /*
      When user changes bet value
      @Params event: [Click]
    */
    function handleInputChange(event) {
      var newBetnBtn = myBetBtn.replace(/[0-9]+/, '')
      // If range, change text box and update button
      if (event.currentTarget && event.currentTarget.type === 'range') {
        document.getElementById('raiseValue').value = event.currentTarget.value
        setBetbtn(newBetnBtn+ event.currentTarget.value)
      // Else change range and update button
      } else {
        var target = document.getElementById('raiseValue')
        var newValue = parseInt(target.value)
        var minValue = parseInt(target.min)
        var sentVal = newValue >= minValue ? Math.min(newValue, parseInt(target.max)) : minValue
        document.getElementById('raiseRange').value = sentVal
        setBetbtn(newBetnBtn + sentVal)
      }
    }

  return (
    <div className='hero-chat-btns'>
      <div className='chatBox'>
        <div className='chatText' id='chatTextBox'>
          <FlatList
          contentContainerStyle={{ height: 'inherit'}}
          style={{ flexDirection: "column", flexShrink: 0}}
          data={chatMessages}
          initialNumToRender={100}
          renderItem={({ item }) => <ChatItem data={item} />}
          />
        </div>
        <div className='chatInput'>
          <input type="text" className='chatInputText' id='chatInput' spellCheck='false' onKeyDown={handleKeyPress}/>
          <button className='chatInputSend' onClick={handleSendMessage}>{t('send')}</button>
        </div>
      </div>
      <div className="hero-actions-btn">
        <table>
          <tbody>
            <tr>
              <td><button data-action="fold" onClick={handleAction}>{props.foldbtn}</button></td>
              <td><button data-action="call" onClick={handleAction}>{props.callbtn}</button></td>
              <td><button data-action="raise" id='raisebtn' onClick={handleAction}>{myBetBtn}</button></td>
            </tr>
            <tr>
              <td colSpan="2">
                <input type='range' className='raiseRange' id="raiseRange" onChange={handleInputChange}></input>
              </td>
              <td>
                <input type="number" id="raiseValue" className='raiseValue'onChange={handleInputChange}></input>
              </td>
            </tr>
          </tbody>
        </table>
      </div> 
    </div>
  );
}
export default HeroActionButtons;