import React from 'react';
import { useTranslation } from 'react-i18next';

const JokerPopup= (props) =>{
  const { t } = useTranslation();

	/*
			Returns all possible cards formatted
	*/
	function cardMosaic() {
		var allCards = []
		// For every rank, for every suit, push 'rank_suit'
		for (var rank = 0; rank<13; rank++) {
			for (var suit = 0; suit < 4; suit++) {
					allCards.push(rank+'_'+suit)
			}
		}
		return allCards
	}

	    
    /*
        When user clicks card button on joker proposal
        @Params event: [Click]
    */
		function handleJokerChangeClick(event) {
			var data = {
				hero: props.rumbleId,
				card: event.currentTarget.id
			}
			// Hide joker popup and emit data
			document.getElementById('jokerpopup').style.display = 'none'
			props.socket.emit("joker-pick", data);
		}

  return (
    <div id="jokerpopup" className='overlay overlay-joker'>
        <div className='popup joker-popup'>
            <div className='content'>{t('you_have_joker')}</div>
            <div className='all-cards-div'>
                {cardMosaic().map((card) => <img className='minicard' id={card} alt={card} onClick={handleJokerChangeClick} src={props.UxUtil.cardSrc(card)} />)}
            </div>
        </div>
    </div>
  );
}
export default JokerPopup;


