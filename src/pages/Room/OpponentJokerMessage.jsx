import React from 'react';
import { useTranslation } from 'react-i18next';

const OpponentJokerMessage= () =>{
  const { t } = useTranslation();

  return (
  <div id="opponent-joker-msg" className="custom-message">
    <p>{t('opponent_changing_joker')}</p>
  </div>
  );
}
export default OpponentJokerMessage;
