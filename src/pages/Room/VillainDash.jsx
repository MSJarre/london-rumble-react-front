import React, {useEffect} from 'react';

const VillainDash= (props) =>{

  useEffect(() => {
    document.getElementById('vBetV').classList.toggle("transparent", props.VillainBet === 0);
}, [props.VillainBet]);

  return (
  <div className="villain-dash dash">
    <table>
      <tbody>
        <tr>
          <td colSpan='5' className='villain-stack stack'>
            <p>{props.VillainStack}</p>
          </td>
        </tr>
        <tr id='villain-cards' className='villain-cards'>
          <td className='villain-private private'>
            <img className='card' alt={props.VillainPrivate} src={props.UxUtil.cardSrc(props.VillainPrivate)} />
          </td>
          <td className='villain-public villain-0'>
            <img className='card card0' alt={props.VillainPublic[0]} src={props.UxUtil.cardSrc(props.VillainPublic[0])} />
          </td>
          <td className='villain-public villain-1'>
            <img className='card card1' alt={props.VillainPublic[1]} src={props.UxUtil.cardSrc(props.VillainPublic[1])} />
          </td>
          <td className='villain-public villain-2'>
            <img className='card card2' alt={props.VillainPublic[2]} src={props.UxUtil.cardSrc(props.VillainPublic[2])} />
          </td>
          <td className='villain-public villain-3'>
            <img className='card card3' alt={props.VillainPublic[3]} src={props.UxUtil.cardSrc(props.VillainPublic[3])} />
          </td>
        </tr>
        <tr>
          <td colSpan='5' className='villain-bet bet'>
            <p id='vBetV'>{props.VillainBet}</p>
          </td>
        </tr>
      </tbody>
    </table>
  </div>
  );
}
export default VillainDash;