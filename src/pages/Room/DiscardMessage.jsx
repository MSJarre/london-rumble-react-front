import React from 'react';
import { useTranslation } from 'react-i18next';

const DiscardMessage= () =>{
  const { t } = useTranslation();

  return (
  <div id="discard-msg" className="custom-message">
    <p>{t('your_time_to_discard')}</p>
  </div>
  );
}
export default DiscardMessage;
