import React, { useState, useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';
import UxUtil from './UxUtil';

const Rematch= (props) =>{
  const { t } = useTranslation();
  const [rematchSentence, SetRematchSentence] = useState(t('you_lost'))

  useEffect(() => {
    props.socket.on('startNewTable', (data) => {
      displayRematchPopup(data)
    })

    return () => {
      props.socket.off('startNewTable')
    }
  });

  /*
      Display the rematch popup
      @Params data: {winner: Integer, room: String}
  */
  function displayRematchPopup(data) {
    if (data.winner === props.rumbleId) {
        SetRematchSentence('you_won')
        UxUtil.playSound('winner')
    } else {
        SetRematchSentence('you_lost')
        UxUtil.playSound('loser')
    }
    document.getElementById('rematch').style.display = 'flex'
    // Set data-attribute with room id
    document.getElementById('rematch-btn').setAttribute('data-roomid', data.room)
  }

  /*
    When user clicks on rematch --> Redirect
    @Params event: [Click]
  */
  function handleRematch(event) {
    window.location = window.location.origin+"/room/?id="+event.currentTarget.getAttribute('data-roomid')
  }

  return (
    <div id="rematch" className='overlay welcome-page rematch-overlay'>
        <div className='rumble-title'>
            <p>{t(rematchSentence)}</p>
        </div>
        <div className='home-page-buttons'>
            <button className="start-game" id="rematch-btn" data-roomid="" onClick={handleRematch}>{t('rematch')}</button>
            <Link to={'/'} className='start-game' id='back-to-lobby'>{t('lobby')}</Link>
        </div>
    </div>
  );
}
export default Rematch;