import React from 'react';
import {useNavigate} from 'react-router-dom';
import {useTranslation} from 'react-i18next';

const AliasSelector= () =>{
  const navigate = useNavigate();
  const {t} = useTranslation();


  function handleCreation() {
    const rumbleName = document.getElementById('rumbleUserName').value;
    if (rumbleName !== '') {
      localStorage.rumbleUserName = rumbleName;
      navigate('/');
    }
  }

  function handleKeyPress(event) {
    if (event.keyCode === 13) {
      handleCreation();
    }
  }

  return (
    <form className='welcome-page'>
      <div className='rumble-title'>
        <p>{t('choose_nickname')}</p>
      </div>
      <div className="form__group gameName">
        <input type="input" autoFocus className="form__field"
          placeholder={t('my_nickname')} required name="rumbleUserName"
          id='rumbleUserName' spellCheck='false' onKeyDown={handleKeyPress}/>
      </div>
      <div className='home-page-buttons'>
        <button className='start-game' id='join-room' onClick={handleCreation}>
          {t('lets_go')}
        </button>
      </div>
    </form>
  );
};
export default AliasSelector;
